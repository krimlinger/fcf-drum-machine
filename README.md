# fcf-drum-machine

A virtual drum build in React.js and based on a FCF's project.

The sound bank is the one used in the example provided by FCF
[here](https://codepen.io/freeCodeCamp/pen/MJyNMd?editors=0110).
