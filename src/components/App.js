import React, { useState } from 'react';
import './App.scss';
import { Container, Row, Col } from '../BootstrapGrid';
import DrumPadContainer from './DrumPadContainer';
import ControlsContainer from './ControlsContainer';
import { HEATER_TYPE, PIANO_TYPE } from '../sounds';

export default function App() {
  const [soundType, setSoundType] = useState(HEATER_TYPE);
  const [displayMessage, setDisplayMessage] = useState('Click the drums or use your keyboard.');
  const [volume, setVolume] = useState(50);

  const handleSoundTypeChange = () => {
    if (soundType === HEATER_TYPE) {
      setSoundType(PIANO_TYPE);
      setDisplayMessage('Using piano sounds.');
    } else {
      setSoundType(HEATER_TYPE);
      setDisplayMessage('Using heater sounds.');
    }
  };

  const handleDrumActivate = (id) => {
    setDisplayMessage(id.replace(/-/g, ' '));
  };

  const handleVolumeChange = (ev) => {
    setVolume(ev.target.value);
  };

  return (
    <Container
      as="main"
      id="drum-machine"
      className="bg-light border rounded p-3"
    >
      <Row className="justify-content-center">
        <Col xs="auto">
          <DrumPadContainer
            kind={soundType}
            volume={volume}
            onDrumActivate={handleDrumActivate}
          />
        </Col>
        <Col xs="auto">
          <ControlsContainer
            displayMessage={displayMessage}
            kind={soundType}
            volume={volume}
            onSoundTypeChange={handleSoundTypeChange}
            onVolumeChange={handleVolumeChange}
          />
        </Col>
      </Row>
    </Container>
  );
}
