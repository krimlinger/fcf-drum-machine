import React from 'react';
import Form from 'react-bootstrap/Form';
import { HEATER_TYPE, PIANO_TYPE } from '../sounds';

export default function ControlsContainer({
  displayMessage, kind, volume, onSoundTypeChange, onVolumeChange }) {

  return (
    <div className="d-flex flex-column justify-content-between align-items-center">
      <Form>
        <p>Choose your sound type:</p>
        <Form.Check
          custom
          type="radio"
          id="heater"
          label="Heater"
          checked={kind === HEATER_TYPE}
          onChange={onSoundTypeChange}
        />
        <Form.Check
          custom
          type="radio"
          id="piano"
          label="Piano"
          checked={kind === PIANO_TYPE}
          onChange={onSoundTypeChange}
        />
      </Form>
      <label for="volume">Volume</label>
      <input
        type="range"
        class="custom-range w-50"
        id="volume"
        min="0"
        max="100"
        value={volume}
        onChange={onVolumeChange}
      />
      <div
        className="shadow p-2 mt-2 bg-white rounded"
        id="display"
      >
        {displayMessage}
      </div>
    </div>
  );
}
