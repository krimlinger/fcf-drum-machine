import React from 'react';
import Button from 'react-bootstrap/Button';
import { getSoundsFromKind } from '../sounds';
import './DrumPad.scss';

export default class DrumPad extends React.Component {
  constructor(props) {
    super(props);
    this.buttonRef = React.createRef();
    this.state = { sound: this.getSound(), kind: props.kind };
  }

  getSound(kind=this.props.kind) {
    return getSoundsFromKind(kind)
      .filter(x => x.code === this.props.code)[0];
  }

  handleActivate = () => {
    this.props.onDrumActivate(this.state.sound.id);
    this.buttonRef.current.classList.toggle('active');
    setTimeout(() => this.buttonRef.current.classList.toggle('active'), 500);
    const audio = document.getElementById(this.state.sound.code);
    audio.volume = this.props.volume / 100;
    audio.play();
  }

  handleKeyDown = (event) => {
    if (event.code.toUpperCase() === 'KEY' + this.props.code) {
      this.handleActivate(event);
    }
  };

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyDown);
  }

  componentDidUpdate() {
    if (this.props.kind !== this.state.kind) {
      this.setState({
        sound: this.getSound(this.props.kind),
        kind: this.props.kind
      });
    }
  }

  render() {
    return (
      <div 
        className="drum-pad"
        id={this.state.sound.code.toLowerCase() + '-pad'}
        onClick={this.handleActivate}
      >
        <Button
          variant="secondary"
          id={this.state.sound.code.toLowerCase() + "-button"}
          ref={this.buttonRef}
        >
          {this.props.code}
        </Button>
        <audio
          className="clip"
          id={this.state.sound.code}
          src={this.state.sound.path}
        >
        </audio>
      </div>
    );
  }
}
