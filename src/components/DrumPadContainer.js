import React from 'react';
import { Row, Col } from '../BootstrapGrid';
import DrumPad from './DrumPad'

const DRUMPADS = ['Q', 'W', 'E', 'A', 'S', 'D', 'Z', 'X', 'C'];

function genDrumPadRow(rowIndex, rowSize, kind, volume, onDrumActivate) {
  const startIndex = rowIndex * rowSize;
  const row = DRUMPADS.slice(startIndex, startIndex + rowSize).map((x, i) =>
    <Col xs={3} className="m-1" key={i}>
      <DrumPad
        code={x}
        kind={kind}
        volume={volume}
        onDrumActivate={onDrumActivate}
      />
    </Col>
  );
  return <Row noGutters={true} key={rowIndex}>{row}</Row>;
}

export default function DrumPadContainer({ kind, volume, onDrumActivate }) {
  let rows = [];
  for (const i in [...Array(3).keys()]) {
    rows.push(genDrumPadRow(i, 3, kind, volume, onDrumActivate));
  }
  return rows;
}
