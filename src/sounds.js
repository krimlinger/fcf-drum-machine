const BASEURL = 'https://s3.amazonaws.com/freecodecamp';

function joinFullPath(obj) {
  return Object.assign({}, obj, { path: `${BASEURL}/${obj.path}` });
}

export function getSoundsFromKind(kind) {
  if (kind === HEATER_TYPE) {
    return HEATER;
  }
  return PIANO;
}

export const HEATER_TYPE = 1;

export const HEATER = [{
  code: 'Q',
  id: 'Heather-1',
  path: 'drums/Heater-1.mp3',
}, {
  code: 'W',
  id: 'Heather-2',
  path: 'drums/Heater-2.mp3',
}, {
  code: 'E',
  id: 'Heather-3',
  path: 'drums/Heater-3.mp3',
}, {
  code: 'A',
  id: 'Heather-4',
  path: 'drums/Heater-4_1.mp3',
}, {
  code: 'S',
  id: 'Clap',
  path: 'drums/Heater-6.mp3',
}, {
  code: 'D',
  id: 'Open-HH',
  path: 'drums/Dsc_Oh.mp3',
}, {
  code: 'Z',
  id: "Kick-n'-Hat",
  path: 'drums/Kick_n_Hat.mp3',
}, {
  code: 'X',
  id: 'Kick',
  path: 'drums/RP4_KICK_1.mp3',
}, {
  code: 'C',
  id: 'Closed-HH',
  path: 'drums/Cev_H2.mp3',
}].map(joinFullPath);

export const PIANO_TYPE = 2;

export const PIANO = [{
  code: 'Q',
  id: 'Chord-1',
  path: 'drums/Chord_1.mp3',
}, {
  code: 'W',
  id: 'Chord-2',
  path: 'drums/Chord_2.mp3',
}, {
  code: 'E',
  id: 'Chord-3',
  path: 'drums/Chord_3.mp3',
}, {
  code: 'A',
  id: 'Shaker',
  path: 'drums/Give_us_a_light.mp3',
}, {
  code: 'S',
  id: 'Open-HH',
  path: 'drums/Dry_Ohh.mp3',
}, {
  code: 'D',
  id: 'Closed-HH',
  path: 'drums/Bld_H1.mp3',
}, {
  code: 'Z',
  id: "Punchy-Kick",
  path: 'drums/punchy_kick_1.mp3',
}, {
  code: 'X',
  id: 'Side-Stick',
  path: 'drums/side_stick_1.mp3',
}, {
  code: 'C',
  id: 'Snare',
  path: 'drums/Brk_Snr.mp3',
}].map(joinFullPath);
